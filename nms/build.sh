#!/bin/bash
while getopts ':t:' OPTION
do
  case "$OPTION" in
    t)
      OUT_IMG_NAME=$OPTARG
      ;;
    \?)
      echo "usage: $0 -t image_name"
      exit
      ;;
  esac
done

if [ -z "${OUT_IMG_NAME}" ]; then
  echo "output image name is required"
  exit
fi

if [[ -f $(pwd)/nginx-repo.crt && -f $(pwd)/nginx-repo.crt ]]; then
  DOCKER_BUILDKIT=1 docker build  --no-cache --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt -t $OUT_IMG_NAME .
  #DOCKER_BUILDKIT=1 docker build  --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt -t $OUT_IMG_NAME .
else
  echo "nginx-repo.crt and/or nginx-repo.key does not exists in $(pwd)"
  exit 1
fi
