#!/bin/sh

nginx

sleep 2

_ARGS="--server-host $NMS_HOST --server-grpcport $NMS_PORT"

if [ ! -z "$INSTANCE_GROUP" ]; then
  _ARGS="${_ARGS} --instance-group $INSTANCE_GROUP"
fi

nginx-agent $_ARGS
