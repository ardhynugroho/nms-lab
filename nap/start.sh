#!/bin/sh

/bin/su -s /bin/sh -c "/usr/share/ts/bin/bd-socket-plugin tmm_count 4 proc_cpuinfo_cpu_mhz 2000000 total_xml_memory 307200000 total_umu_max_size 3129344 sys_max_account_id 1024 no_static_config 2>&1 >> /var/log/app_protect/bd-socket-plugin.log &" nginx

nginx

sleep 2

_ARGS="--server-host $NMS_HOST --server-grpcport $NMS_PORT"

if [ ! -z "$INSTANCE_GROUP" ]; then
  _ARGS="${_ARGS} --instance-group $INSTANCE_GROUP"
fi

nginx-agent $_ARGS
