FROM debian:bullseye-slim

ARG NMS_ADDR

RUN --mount=type=secret,id=nginx-crt,dst=/etc/ssl/nginx/nginx-repo.crt,mode=0644 \
    --mount=type=secret,id=nginx-key,dst=/etc/ssl/nginx/nginx-repo.key,mode=0644 \

set -x \

# Install prerequisite packages:
&& apt-get update && apt-get install -y apt-transport-https lsb-release ca-certificates wget gnupg2 \

# Download and add the NGINX signing keys:
&& wget https://cs.nginx.com/static/keys/nginx_signing.key && apt-key add nginx_signing.key \
&& wget https://cs.nginx.com/static/keys/app-protect-security-updates.key && apt-key add app-protect-security-updates.key \

# Add NGINX Plus repository:
&& printf "deb https://pkgs.nginx.com/plus/debian `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nginx-plus.list \

# Add NGINX App Protect WAF repositories:
&& printf "deb https://pkgs.nginx.com/app-protect/debian `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/nginx-app-protect.list \
&& printf "deb https://pkgs.nginx.com/app-protect-security-updates/debian `lsb_release -cs` nginx-plus\n" | tee /etc/apt/sources.list.d/app-protect-security-updates.list \

# Download the apt configuration to `/etc/apt/apt.conf.d`:
&& wget -P /etc/apt/apt.conf.d https://cs.nginx.com/static/files/90pkgs-nginx \

# Update the repository and install the most recent version of the NGINX App Protect WAF package (which includes NGINX Plus):
&& apt-get update \
&& DEBIAN_FRONTEND="noninteractive" apt-get install -y app-protect nginx-plus-module-njs \

# Forward request logs to Docker log collector:
&& ln -sf /dev/stdout /var/log/nginx/access.log \
&& ln -sf /dev/stderr /var/log/nginx/error.log \

# install nginx agent
&& wget --no-check-certificate https://$NMS_ADDR/install/nginx-agent -O - | sh \
&& echo "nginx_app_protect:\n  report_interval: 15s" >> /etc/nginx-agent/nginx-agent.conf \
&& echo "nap_monitoring:\n  collector_buffer_size: 50000\n  processor_buffer_size: 50000\n  syslog_ip: \"127.0.0.1\"\n  syslog_port: 514" >> /etc/nginx-agent/nginx-agent.conf

# Copy configuration files:
COPY nginx.conf /etc/nginx/
COPY log_sm.json /etc/app_protect/conf/
COPY start.sh /

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["sh","/start.sh"]
