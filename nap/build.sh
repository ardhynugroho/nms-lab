#!/bin/bash
while getopts ':t:s:' OPTION
do
  case "$OPTION" in
    t)
      OUT_IMG_NAME=$OPTARG
      ;;
    s)
      NMS_ADDR=$OPTARG
      ;;
    \?)
      echo "usage: $0 -t image_name -s nms_address"
      exit
      ;;
  esac
done

if [ -z "${OUT_IMG_NAME}" ]; then
  echo "output image name is required"
  exit
fi
if [ -s "${NMS_ADDR}" ]; then
  echo "NMS hostname or IP address  is required"
  exit
fi

if [[ -f $(pwd)/nginx-repo.crt && -f $(pwd)/nginx-repo.crt ]]; then
  DOCKER_BUILDKIT=1 docker build  --no-cache --secret id=nginx-key,src=nginx-repo.key --secret id=nginx-crt,src=nginx-repo.crt --build-arg NMS_ADDR=$NMS_ADDR -t $OUT_IMG_NAME .
else
  echo "nginx-repo.crt and/or nginx-repo.key does not exists in $(pwd)"
  exit 1
fi
