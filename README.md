# NMS Lab

Simple NMS lab running on docker

```mermaid
graph TD
subgraph "Docker environment"
CL(Client) --- LB(LB<br>NGINX Plus<br>:80) --- GW1(APIGW) --- APP(httpbin-app)
LB --- GW2(APIGW) --- APP
CL --- NAP(LB<br>NGINX AppProtect<br>:81) --- GW1
NAP --- GW2
NMS(NGINX Management Suite) -.- LB
NMS -.- NAP
NMS -.- GW1
NMS -.- GW2
end
```

## Preparation

1. Clone repo
2. Get NMS trial license & NGINX Plus trial license
3. Copy NMS .crt & .key file to nms directory
4. Copt NGINX Plus .crt & .key file to nplus and nap directory

## Build NMS Docker image

```
cd nms
chmod +x build.sh
./build.sh -t nms
```

## Run NMS

Deploy Clickhouse & NMS container

```
docker compose up -d clickhouse nms
```

Check with web browser if you can access the NMS GUI

## Build NGINX Plus Docker Image

You will need NMS to get started first, because NGINX Plus & NGINX AppProtect image will be built with NGINX Agent installed.

Find your Docker host IP address. The address will be used for NGINX Agent installation.
Dont using `localhost` or `127.0.0.1`

Use command `./build.sh -t nplus -s DOCKER__HOST_IP_ADDRESS` to start the build process.

```
cd nplus
chmod +x build.sh
./build.sh -t nplus -s 10.1.1.2
```

## Build NGINX App Protect (NAP) Docker image

```
cd nap
chmod +x build.sh
./build.sh -t nap -s 10.1.1.2
```

## Deploy all the container

Deploy all remaining container in the `docker-compose.yaml` file.
Including backend application `httpbin-app`.

```
docker compose up -d
```

## Deploy API Gateway for `httpbin-app`

Deploy API gateway via NMS API

```
./scripts/end2end_deploy.sh
```

## The client container

Client container uses `wrk` or `rtapi` to generate web traffic.
And there is `nikto` to generate web-scan attack.

Enter the container command

```
docker compose exec client bash
```

## Delete API gateway

Delete the API gateway configuration in using NMS API

```
./scripts/end2end_delete.sh
```
